<?php

namespace Drupal\ai_interpolator_screenshot;

use Drupal\ai_interpolator\Exceptions\AiInterpolatorRequestErrorException;
use Drupal\ai_interpolator_screenshot\Form\ScreenshotConfigForm;
use Drupal\Core\Config\ConfigFactory;
use ScreenshotOne\Sdk\Client;
use ScreenshotOne\Sdk\TakeOptions;

/**
 * SERP API.
 */
class ScreenshotOne {

  /**
   * API Key.
   */
  private string $apiKey;

  /**
   * Secret Key.
   */
  private string $secretKey;

  /**
   * Constructs a new SERP object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   */
  public function __construct( ConfigFactory $configFactory) {
    $config = $configFactory->get(ScreenshotConfigForm::CONFIG_NAME);
    $this->apiKey = $config->get('api_key') ?? '';
    $this->secretKey = $config->get('secret_key') ?? '';
  }

  /**
   * Screenshot API Call.
   *
   * @param string $uri
   *   The uri to screenshot.
   * @param array $parameters
   *   The parameters for the call.
   *
   * @return string
   *   A binary for an image.
   */
  public function screenshotUrl($url, array $parameters) {
    if (empty($url) || !filter_var($url, FILTER_VALIDATE_URL)) {
      throw new AiInterpolatorRequestErrorException('You have to give an url');
    }
    $client = new Client($this->apiKey, $this->secretKey);
    $options = TakeOptions::url($url);
    $options->fullPage($parameters['full_page'] ?? TRUE);
    $options->delay($parameters['delay'] ?? 0);
    $options->viewportWidth($parameters['width'] ?? 1280);
    $options->viewportHeight($parameters['height'] ?? 800);
    $options->format($parameters['format'] ?? 'jpg');
    $options->imageQuality($parameters['image_quality'] ?? 80);
    if (!empty($parameters['selector'])) {
      $options->selector($parameters['selector']);
    }
    if (!empty($parameters['geolocation_latitude'])) {
      $options->geolocationLatitude($parameters['geolocation_latitude']);
    }
    if (!empty($parameters['geolocation_longitude'])) {
      $options->geolocationLongitude($parameters['geolocation_longitude']);
    }

    return $client->take($options);
  }

}
