<?php

namespace Drupal\ai_interpolator_screenshot\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure ScreenshotConfigForm API access.
 */
class ScreenshotConfigForm extends ConfigFormBase {

  /**
   * Config settings.
   */
  const CONFIG_NAME = 'ai_interpolator_screenshot.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ai_interpolator_screenshot';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::CONFIG_NAME);

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Screenshot One API Key'),
      '#description' => $this->t('Can be found and generated <a href="https://app.screenshotone.com/access?via=7kunwb2yqcvwhdnkqvkm" target="_blank">here</a>.'),
      '#default_value' => $config->get('api_key'),
    ];

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Screenshot One Secret Key'),
      '#description' => $this->t('Can be found and generated <a href="https://app.screenshotone.com/access?via=7kunwb2yqcvwhdnkqvkm" target="_blank">here</a>.'),
      '#default_value' => $config->get('secret_key'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::CONFIG_NAME)
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('secret_key', $form_state->getValue('secret_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
