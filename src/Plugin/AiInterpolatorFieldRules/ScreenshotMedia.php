<?php

namespace Drupal\ai_interpolator_screenshot\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_screenshot\ScreenshotOne;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Utility\Token;
use Drupal\file\Entity\File;
use Drupal\file\FileRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The rules for a screenshot media field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_screenshot_media",
 *   title = @Translation("Screenshot One Media"),
 *   field_rule = "entity_reference",
 *   target = "media"
 * )
 */
class ScreenshotMedia extends AiInterpolatorFieldRule implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'Screenshot One Media';

  /**
   * The pixabay api service.
   */
  public ScreenshotOne $screenshotOne;

  /**
   * The entity type manager.
   */
  public EntityTypeManagerInterface $entityManager;

  /**
   * The File System interface.
   */
  public FileSystemInterface $fileSystem;

  /**
   * The File Repo.
   */
  public FileRepositoryInterface $fileRepo;

  /**
   * The token system to replace and generate paths.
   */
  public Token $token;

  /**
   * The current user.
   */
  public AccountProxyInterface $currentUser;

  /**
   * The logger channel.
   */
  public LoggerChannelFactoryInterface $loggerChannel;

  /**
   * Cache service.
   */
  protected CacheBackendInterface $cache;

  /**
   * The field manager.
   */
  protected EntityFieldManagerInterface $fieldManager;

  /**
   * Construct an image field.
   *
   * @param array $configuration
   *   Inherited configuration.
   * @param string $plugin_id
   *   Inherited plugin id.
   * @param mixed $plugin_definition
   *   Inherited plugin definition.
   * @param \Drupal\ai_interpolator_screenshot\ScreenshotOne $screenshotOne
   *   The screenshot one service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityManager
   *   The entity type manager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The File system interface.
   * @param \Drupal\file\FileRepositoryInterface $fileRepo
   *   The File repo.
   * @param \Drupal\Core\Utility\Token $token
   *   The token system.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannel
   *   The logger channel interface.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $fieldManager
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ScreenshotOne $screenshotOne,
    EntityTypeManagerInterface $entityManager,
    FileSystemInterface $fileSystem,
    FileRepositoryInterface $fileRepo,
    Token $token,
    AccountProxyInterface $currentUser,
    LoggerChannelFactoryInterface $loggerChannel,
    CacheBackendInterface $cache,
    EntityFieldManagerInterface $fieldManager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->screenshotOne = $screenshotOne;
    $this->entityManager = $entityManager;
    $this->fileSystem = $fileSystem;
    $this->fileRepo = $fileRepo;
    $this->token = $token;
    $this->currentUser = $currentUser;
    $this->loggerChannel = $loggerChannel;
    $this->cache = $cache;
    $this->fieldManager = $fieldManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('ai_interpolator_screenshot.api'),
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('file.repository'),
      $container->get('token'),
      $container->get('current_user'),
      $container->get('logger.factory'),
      $container->get('cache.default'),
      $container->get('entity_field.manager'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function advancedMode() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function allowedInputs() {
    return [
      'link',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function helpText() {
    return $this->t("Screenshot an image media based on a url.");
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "";
  }

  /**
   * {@inheritDoc}
   */
  public function extraFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form['interpolator_screenshot_width'] = [
      '#type' => 'number',
      '#title' => 'Screenshot Width',
      '#required' => TRUE,
      '#description' => $this->t('The width of the screenshot'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_screenshot_width', 1280),
    ];

    $form['interpolator_screenshot_height'] = [
      '#type' => 'number',
      '#title' => 'Screenshot Height',
      '#required' => TRUE,
      '#description' => $this->t('The height of the screenshot'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_screenshot_height', 1280),
    ];

    $form['interpolator_screenshot_delay'] = [
      '#type' => 'number',
      '#title' => 'Screenshot Delay',
      '#required' => TRUE,
      '#description' => $this->t('Delay of taking the screenshot'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_screenshot_delay', 0),
    ];

    $form['interpolator_screenshot_full_page'] = [
      '#type' => 'checkbox',
      '#title' => 'Fullpage',
      '#description' => $this->t('Take a full page screenshot'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_screenshot_full_page', FALSE),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $values = [];
    if (!empty($entity->{$interpolatorConfig['base_field']}->uri)) {
      foreach ($entity->get($interpolatorConfig['base_field']) as $wrapperEntity) {
        $link = $wrapperEntity->uri;
        // We generate later, so we don't run out of memory.
        $values[] = $link;
      }
    }
    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    if (filter_var($value, FILTER_VALIDATE_URL)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    $config = $fieldDefinition->getConfig($entity->bundle())->getSettings();
    $fileField = $this->getMediaField(key($config['handler_settings']['target_bundles']));
    $imageConfig = $fileField->getConfig($entity->bundle())->getSettings();
    $specs = $fieldDefinition->getConfig($entity->bundle())->getThirdPartySettings('ai_interpolator');
    $params = $this->getParams($specs);

    // Transform string to boolean.
    $mediaEntities = [];

    // Successful counter, to only download as many as max.
    $successFul = 0;
    foreach ($values as $value) {
      // Get filename from previewURL.
      $fileName = 'screenshot_' . md5($value) . '.jpg';
      // Everything validated, then we prepare the file path to save to.
      $filePath = $this->token->replace($imageConfig['uri_scheme'] . '://' . rtrim($imageConfig['file_directory'], '/')) . '/' . $fileName;
      // Get the screenshot.
      $fileString = $this->screenshotOne->screenshotUrl($value, $params);
      // Create file entity from string.
      $file = $this->generateFileFromString($fileString, $filePath);
      // If we can save, we attach it.
      if ($file) {
        $media = $this->generateMediaFromFile($file, key($config['handler_settings']['target_bundles']), $this->t('Screenshot %website', [
          '%website' => $value,
        ]));
        $mediaEntities[] = $media;

        $successFul++;
        // If we have enough images, give up.
        if ($successFul == $fieldDefinition->getFieldStorageDefinition()->getCardinality()) {
          break;
        }
      }
    }

    // Then set the value.
    $entity->set($fieldDefinition->getName(), $mediaEntities);
  }

  /**
   * Generate params from config.
   *
   * @param array $specs
   *   The config spec.
   *
   * @return array
   *   The params to screenshot.
   */
  private function getParams(array $specs) {
    $params = [];
    foreach ($specs as $key => $val) {
      $params[str_replace('interpolator_screenshot_', '', $key)] = $val;
    }
    return $params;
  }

  /**
   * Generate a media entity from a file.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file.
   * @param string $mediaType
   *   The media type.
   * @param string $mediaName
   *   The media name.
   *
   * @return \Drupal\media\MediaInterface
   *   The media entity.
   */
  private function generateMediaFromFile(File $file, $mediaType, $mediaName) {
    $sourceField = $this->getMediaField($mediaType);
    $fileField = $sourceField->getName();
    $mediaStorage = $this->entityManager->getStorage('media');
    $imageConfig = $sourceField->getConfig($mediaType)->getSettings();
    if (!$imageConfig) {
      return [];
    }
    // Get resolution.
    $resolution = getimagesize($file->uri->value);

    // Prepare for Media.
    $fileForMedia = [
      'target_id' => $file->id(),
      'alt' => $imageConfig['default_image']['alt'] ?? '',
      'title' => $imageConfig['default_image']['title'] ?? '',
      'width' => $resolution[0],
      'height' => $resolution[1],
    ];

    /** @var \Drupal\media\Entity\Media */
    $media = $mediaStorage->create([
      'name' => $mediaName,
      'bundle' => $mediaType,
      $fileField => $fileForMedia,
    ]);
    $media->save();

    return $media;
  }

  /**
   * Generate a file entity.
   *
   * @param string $binary
   *   The source binary.
   * @param string $dest
   *   The destination.
   *
   * @return \Drupal\file\FileInterface|false
   *   The file or false on failure.
   */
  private function generateFileFromString(string $binary, string $dest) {
    // Calculate path.
    $fileName = basename($dest);
    $path = substr($dest, 0, -(strlen($fileName) + 1));
    // Create directory if not existsing.
    $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);
    $file = $this->fileRepo->writeData($binary, $dest, FileSystemInterface::EXISTS_RENAME);
    if ($file) {
      return $file;
    }
    return FALSE;
  }

  /**
   * Get the field definition of the medias field.
   *
   * @param string $mediaType
   *   The media type.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   *   The field definition.
   */
  private function getMediaField($mediaType) {
    $mediaStorage = $this->entityManager->getStorage('media');
    $mediaTypeInterface = $this->entityManager->getStorage('media_type')->load($mediaType);
    $media = $mediaStorage->create([
      'name' => 'tmp',
      'bundle' => $mediaType,
    ]);
    $sourceField = $media->getSource()->getSourceFieldDefinition($mediaTypeInterface);
    return $sourceField;
  }

}
